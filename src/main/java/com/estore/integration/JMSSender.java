package com.estore.integration;

public interface JMSSender {
	public void sendJMSMessage (String text);
}
