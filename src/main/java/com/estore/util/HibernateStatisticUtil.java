package com.estore.util;

import java.util.Arrays;

import org.hibernate.SessionFactory;
import org.hibernate.stat.SecondLevelCacheStatistics;
import org.hibernate.stat.Statistics;

public class HibernateStatisticUtil {
	
	 public static void printStatistics(SessionFactory sessionFactory) {
	        Statistics stat = sessionFactory.getStatistics();
	        String regions[] = stat.getSecondLevelCacheRegionNames();
	        System.out.println(Arrays.toString(regions));
	        for(String regionName:regions) {
	            SecondLevelCacheStatistics stat2 = stat.getSecondLevelCacheStatistics(regionName);
	            System.out.println("2nd Level Cache(" +regionName+") Put Count: "+stat2.getPutCount());
	            System.out.println("2nd Level Cache(" +regionName+") HIt Count: "+stat2.getHitCount());
	            System.out.println("2nd Level Cache(" +regionName+") Miss Count: "+stat2.getMissCount());
	        }
	    }
}
