package com.estore.dao;


import java.io.IOException;

import com.estore.model.Users;

public interface UserDao {
    void update(Users user);
    Users getUserByUsername(String username);
}

