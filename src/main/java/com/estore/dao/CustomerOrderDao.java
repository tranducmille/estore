package com.estore.dao;

import java.util.List;

import com.estore.model.CustomerOrder;

public interface CustomerOrderDao {

    void addCustomerOrder(CustomerOrder customerOrder);
    
    public List<CustomerOrder> getAllOrders();
    
    public CustomerOrder loadById(int id);
}
