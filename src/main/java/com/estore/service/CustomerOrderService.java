package com.estore.service;


import java.util.List;

import com.estore.model.CustomerOrder;

public interface CustomerOrderService {

    void addCustomerOrder(CustomerOrder customerOrder);

    double getCustomerOrderGrandTotal(int cartId);
    
    public List<CustomerOrder> getAllOrders();
    
    public CustomerOrder loadById(int id);
    
}
