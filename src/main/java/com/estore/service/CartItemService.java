package com.estore.service;


import com.estore.model.Cart;
import com.estore.model.CartItem;
import com.estore.model.CustomerOrder;

public interface CartItemService {

    void addCartItem(CartItem cartItem);

    void removeCartItem(CartItem cartItem);

    void removeAllCartItems(Cart cart);
    
    void removeAllCartItems(CustomerOrder order);

    CartItem getCartItemByProductId(int productId);

}
