package com.estore.controller.admin;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.estore.model.Customer;
import com.estore.model.CustomerOrder;
import com.estore.model.Product;
import com.estore.service.CartService;
import com.estore.service.CustomerOrderService;
import com.estore.service.CustomerService;
import com.estore.service.ProductService;

@Controller
@RequestMapping("/admin")
public class AdminHome {

    @Autowired
    private ProductService productService;

    @Autowired
    private CustomerService customerService;
    
    @Autowired
    private CustomerOrderService customerOrderService;
    
    @Autowired
    private CartService cartService;

    @RequestMapping
    public String adminPage(Model model){
    	 List<Product> products = productService.getProductList();
         model.addAttribute("noofproducts", products.size());
         
         Long totalcustomers = cartService.getTotalNoOfCart();
         model.addAttribute("totalcustomers", totalcustomers);
         
         List<CustomerOrder> top5order = customerOrderService.getAllOrders();
         model.addAttribute("top5order", top5order);
         
         return "adminnewhome";
    }
    @RequestMapping("/productInventory")
    public String productInventory(Model model){
        List<Product> products = productService.getProductList();
        model.addAttribute("products", products);

        return "productInventory";
    }

    @RequestMapping("/customer")
    public String customerManagerment(Model model){

        List<Customer> customerList = customerService.getAllCustomers();
        model.addAttribute("customerList", customerList);

        return "admincustomerManagement";
    }
    
    @RequestMapping("/customer/enableDisable/{id}")
    public String enableDisable(@PathVariable("id") int id,  Model model){ 
        customerService.enableDisableCustomer(id);
        return "redirect:/admin/customer";
    }


} // The End of Class;
